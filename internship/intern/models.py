from django.db import models

# Create your models here
class Student(models.Model):
    roll_no = models.IntegerField(default=0)
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    mobile_no = models.IntegerField(default=0)
    address = models.CharField(max_length=200)
