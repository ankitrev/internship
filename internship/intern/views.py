from django.shortcuts import render
from django.http import HttpResponse, QueryDict
from django.views.generic import View
from .models import Student
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db import IntegrityError

class StudentData(View):
# Method to show the record of student database

    def get(self,request,id):
        try:
            result=[]
            students=Student.objects.get(id=id)
            res={}
            res['roll_no']=students.roll_no
            res['first_name']=students.first_name
            res['mobile_no']=students.mobile_no
            res['address']=students.address
            result.append(res)
            return HttpResponse(result, status=200)
        except ObjectDoesNotExist:
            print("Either the entry or blog doesn't exist.")

# Method to insert a record into the Student database getting from POST request

    def post(self,request):
        try:
            obj=request.POST
            fname=obj['fn']
            lname=obj['ln']
            mbn=obj['mn'];
            ad=obj['add']
            roll=obj['roll']
            student_entry=Student(roll_no = roll, first_name = fname, last_name = lname, mobile_no = mbn, address = ad)
            student_entry.save()
            return HttpResponse('Successfully inserted the record!')
        except IntegrityError:
            print("this data already exists in the database")


# Method to delete a record from a Student database

    def delete(self,request):
        obj=QueryDict(request.body)
        roll=obj['roll']
        e = Student.objects.filter(roll_no=roll)
        e.delete()
        return HttpResponse('Deleted Successfully', status=200)

# Method to update the record into Student database on the basis of some given id

    def put(self,request,id):
        try:
            print('hii')
            update_req=QueryDict(request.body)
            data=Student.objects.get(id=id)

            if "first_name" in update_req.keys():
                data.first_name=update_req["first_name"]
            if "last_name" in update_req.keys():
                data.last_name=update_req["last_name"]
            if "mobile_no" in update_req.keys():
                data.mobile_no=update_req["mobile_no"]
            if "address" in update_req.keys():
                data.address=update_req["address"]
            data.save()
            return HttpResponse("Successfully updated", status=200)
        except ObjectDoesNotExist:
            return HttpResponse("Object does not exist", status=500)
        except MultipleObjectsReturned:
            return HttpResponse("multiple values returned", status=500)




# Create your views here.
