from django.conf.urls import include, url
from django.contrib import admin
from intern.views import StudentData
from intern import views
urlpatterns = [

    url(r'^admin/', include(admin.site.urls)),
    url(r'^getting/(\d+)/$',StudentData.as_view(),name='get'),
    url(r'^student/', StudentData.as_view()),
    url(r'^update/(\d+)/$',StudentData.as_view(),name='put'),
]
