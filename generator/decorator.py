# It's a asimple decorator

def some(fun):
  def wrapper(*args):
    print args
    return fun(*args)
  return wrapper

@some
def mul(num):
     r=num*num
     return r
k=mul(5)
print(k)
