# Project Title

Internship Project

## Description

This project basically implement some of the basic queries in database with the help of Django API's.

### Prerequisites

1. Python any version must be installed in system.

2. Any type of Database like MySQl , PostgreSQL must be installed.


### Installing

Step 1. First create virtual environment using the command 'virtualenv'

Step 2. Activate the virtual environment using the command 'source bin/activate'

Step 3. Now install django version 1.8 using the command 'pip install django==1.8'


## Running the Project

Step 1. Start the Project
        'django-admin.py startproject Internship'

Step 2. Run the server
        'python manage.py runserver'

Step 3. If server is running correctly the start an app
        'python manage.py startapp intern'                
